from tabulate import tabulate
import psycopg2

conn = psycopg2.connect("host=localhost port=5432 dbname=mydb user=postgres password=secret")
cursor = conn.cursor()

def fetch_all(cursor):
    colnames = [desc[0] for desc in cursor.description]
    records = cursor.fetchall()
    return [{colname:value for colname, value in zip(colnames, record)} for record in records]

""" Вопрос 1. Сколько мужчин и женщин представлено в этом наборе данных? """

cursor.execute(
    """
    SELECT gender, AVG(height), COUNT(gender) AS num
    FROM df
    GROUP BY gender
    """
)
print('Вопрос 1')
print(tabulate(fetch_all(cursor), "keys", "psql"))


"""Вопрос 2. Кто в среднем реже указывает, что употребляет алкоголь – мужчины или женщины?"""

cursor.execute(
    """
    SELECT gender, AVG(alco::int) AS alco
    FROM df
    GROUP BY gender
    """
)
print('Вопрос 2')
print(tabulate(fetch_all(cursor), "keys", "psql"))


"""Вопрос 3. Во сколько раз (округленно) процент курящих среди мужчин больше, чем процент курящих среди женщин?"""

cursor.execute(
    """
    SELECT DISTINCT ROUND(
    (SELECT (AVG(smoke::int)) FROM df WHERE gender = 2) /
    (SELECT (AVG(smoke::int)) FROM df WHERE gender = 1)) as diff
    FROM df
    """
)
print('Вопрос 3')
print(tabulate(fetch_all(cursor), "keys", "psql"))


"""Вопрос 4. Догадайтесь, в чём здесь измеряется возраст, и ответьте, 
на сколько месяцев (примерно) отличаются медианные значения возраста курящих и некурящих."""

cursor.execute(
    """
    SELECT DISTINCT ABS(
        (SELECT median(age) / 30 FROM df WHERE smoke='1') - \
        (SELECT median(age) / 30 FROM df WHERE smoke='0')
    )::int AS diff
    FROM df
    """
)
print('Вопрос 4')
print(tabulate(fetch_all(cursor), "keys", "psql"))


""" Вопрос 5. 
    1. Создайте новый признак age_years – возраст в годах, округлив до целых (round).
       Для данного примера отберите курящих мужчин от 60 до 64 лет включительно.
    2. Категории уровня холестрина на рисунке и в наших данных отличаются. Отображение значений на картинке
       в значения признака cholesterol следующее: 4 ммоль/л -> 1, 5-7 ммоль/л -> 2, 8 ммоль/л -> 3.
    3. Интересуют 2 подвыборки курящих мужчин возраста от 60 до 64 лет включительно:
       первая с верхним артериальным давлением строго меньше 120 мм рт.ст. и концентрацией холестерина – 4 ммоль/л,
       а вторая – с верхним артериальным давлением от 160 (включительно) до 180 мм рт.ст. (не включительно)
       и концентрацией холестерина – 8 ммоль/л.

    Во сколько раз (округленно, round) отличаются доли больных людей (согласно целевому признаку, cardio)
    в этих двух подвыборках?"""

cursor.execute(
    """
    SELECT DISTINCT ROUND(
    (SELECT COUNT(*) FROM df
    WHERE (ap_hi < 120) AND (cholesterol = 1) AND ((cardio::int) = 1)) /
    (SELECT COUNT(*) FROM df
    WHERE (ap_hi < 180) AND (160 <= ap_hi) AND (cholesterol = 3) AND ((cardio::int) = 1)))
    FROM df
    WHERE gender = 2 AND (60 <= ((age::int) / 365)) AND (((age::int) / 365) <= 64)
    """
)
print('Вопрос 5')
print(tabulate(fetch_all(cursor), "keys", "psql"))


"""Вопрос 6. Постройте новый признак – BMI. Для этого надо вес в килограммах поделить на квадрат роста в метрах. 
Нормальными считаются значения BMI от 18.5 до 25. Выберите верные утверждения.
Утверждения:
- Медианный BMI по выборке превышает норму
- У женщин в среднем BMI ниже, чем у мужчин
- У здоровых в среднем BMI выше, чем у больных
- В сегменте здоровых и непьющих мужчин в среднем BMI ближе к норме, чем в сегменте здоровых и непьющих женщин"""

cursor.execute(
    """
    SELECT MEDIAN (weight / ((height / 100) ^ 2))
    FROM df
    """
)
print('Вопрос 6')
print('Медианный BMI по выборке')
print(tabulate(fetch_all(cursor), "keys", "psql"))

cursor.execute(
    """
    SELECT (CASE WHEN gender = 1 THEN 'female' ELSE 'male' END) as sex, 
    AVG(weight / ((height / 100) ^ 2)) as avg_bmi
    FROM df
    GROUP BY gender
    """
)
print('Средний BMI по выборке среди мужчин и женщин')
print(tabulate(fetch_all(cursor), "keys", "psql"))

cursor.execute(
    """
    SELECT (CASE WHEN cardio = True THEN 'sick' ELSE 'healthy' END), 
    AVG(weight / ((height / 100) ^ 2)) as avg_bmi
    FROM df
    GROUP BY cardio
    """
)
print('Средний BMI по выборке среди здоровых и больных')
print(tabulate(fetch_all(cursor), "keys", "psql"))

cursor.execute(
    """
    SELECT (CASE WHEN gender = 1 THEN 'female' ELSE 'male' END) as sex, AVG(weight / ((height / 100) ^ 2)) as avg_bmi
    FROM df
    WHERE cardio = False AND alco = FALSE
    GROUP BY gender
    """
)
print('Средний BMI по выборке среди здоровых и непьющих мужчин и женщин')
print(tabulate(fetch_all(cursor), "keys", "psql"))


"""Вопрос 7. Можно заметить, что в данных много грязи и неточностей.
Отфильтруйте следующие сегменты пациентов:
- указанное нижнее значение артериального давления строго выше верхнего
- рост строго меньше 2.5%-перцентили или строго больше 97.5%-перцентили 
- вес строго меньше 2.5%-перцентили или строго больше 97.5%-перцентили
Сколько процентов данных (округленно) мы выбросили?"""

cursor.execute(
    """
    SELECT DISTINCT ROUND(
    (SELECT COUNT(*) FROM df
    WHERE (ap_lo > ap_hi) 
    OR height < (SELECT PERCENTILE_DISC(0.025) WITHIN GROUP (ORDER BY height) FROM df)
    OR (SELECT PERCENTILE_DISC(0.975) WITHIN GROUP (ORDER BY height) FROM df) < height
    OR weight < (SELECT PERCENTILE_DISC(0.025) WITHIN GROUP (ORDER BY weight) FROM df)
    OR (SELECT PERCENTILE_DISC(0.975) WITHIN GROUP (ORDER BY weight) FROM df) < weight)::numeric /
    (SELECT COUNT(*) FROM df)::numeric * 100) as dirty_percent
    FROM df
    """
)
print('Вопрос 7')
print(tabulate(fetch_all(cursor), "keys", "psql"))